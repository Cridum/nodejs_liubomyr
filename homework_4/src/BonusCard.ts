
import Transaction from "./Transaction";
import {ICard, ITransaction, CurrencyEnum, Balance, IBonusCard} from "./types";
import Card from "./Card";

export default class BonusCard extends Card implements IBonusCard {
	private BonusTransactions: ITransaction[] = [];
	constructor(_balance: Balance = {}, public bonusBalance: Balance = {}) {
		super(_balance);
		this.balance = _balance;
		this.bonusBalance = bonusBalance;
	}

	addTransaction(transaction: any): string {
		this.addBonusTransaction(transaction);
		return super.addTransaction(transaction);
	}

	addBonusTransaction(transaction: ITransaction) {
		const newTransaction = new Transaction(transaction.amount * 0.1, transaction.currency);
		this.BonusTransactions.push(newTransaction);
		this.bonusBalance[newTransaction.currency] = (this.bonusBalance[newTransaction.currency] || 0) + newTransaction.amount;
		return newTransaction.getId;
	}

	getBonusBalance(currency: CurrencyEnum) {
		return currency + ' - ' + (this.bonusBalance?.[currency] ?? 0);
	}
}