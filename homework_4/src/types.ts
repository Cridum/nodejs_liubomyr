import Transaction from "./Transaction";
import exp from "constants";

export enum CurrencyEnum {
    USD = "USD",
    UAH = "UAH",
}

export type Balance = {
    [key in keyof typeof CurrencyEnum as string]?: number;
}

export interface ITransaction {
    amount: number;
    currency: CurrencyEnum;
    get getId(): string;
}

export interface ICard {
    balance: Balance;
    addTransaction(transaction: ITransaction): string;
    addTransaction(transaction: {amount: number, currency: CurrencyEnum}): string;
    getTransactions(id?: string): ITransaction[];
    getBalance(currency: CurrencyEnum): number;
}

export interface IBonusCard extends ICard {
    bonusBalance: Balance;
    addBonusTransaction(transaction: ITransaction): string;
}

export interface IPocket {
    addCard(name: string, card: ICard): void;
    removeCard(name: string): void;
    getCard(name: string): ICard;
    getTotalAmount(currency: CurrencyEnum): number;
}




