
import {ICard, CurrencyEnum, IPocket} from "./types";

export default class Pocket implements IPocket {
	private cards: {[key: string]: ICard} = {};

	addCard(name: string, card: ICard) {
		this.cards[name] = card;
	}

	removeCard(name: string) {
		delete this.cards[name];
	}

	getCard(name: string) {
		return this.cards[name];
	}

	getTotalAmount(currency: CurrencyEnum) {
		return Object.values(this.cards).reduce((acc, card) => acc + card.getBalance(currency), 0);
	}
}