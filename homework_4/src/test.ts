

import Card from "./Card";
import Transaction from "./Transaction";
import {CurrencyEnum} from "./types";
import BonusCard from "./BonusCard";
import Pocket from "./Pocket";

const card1 = new Card({USD: 100});
const card2 = new Card({UAH: 100, USD: 200});
const card3 = new Card();
const bonusCard = new BonusCard({UAH: 100, USD: 200});

const transaction1 = new Transaction(100, CurrencyEnum.USD);
const transaction2 = new Transaction(50, CurrencyEnum.UAH);
const transaction3 = new Transaction(800, CurrencyEnum.USD);

card1.addTransaction(transaction1); //add 100$ to card1
card2.addTransaction(transaction2); //add 50UAH to card2
bonusCard.addTransaction(transaction3); //add 800$ to card2

const pocket = new Pocket();
pocket.addCard('card1', card1);
pocket.addCard('card2', card2);
pocket.addCard('card3', card3);
pocket.addCard('bonusCard', bonusCard);
pocket.removeCard('card3');

console.log(pocket.getTotalAmount(CurrencyEnum.USD));
console.log(pocket.getCard('bonusCard'));
