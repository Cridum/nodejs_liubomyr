
import Transaction from "./Transaction";
import {ICard, ITransaction, CurrencyEnum, Balance} from "./types";

export default class Card implements ICard {
	private transactions: ITransaction[] = [];

	constructor(
		public balance: Balance = {},
		) {
			this.balance = balance;
		}

	addTransaction(transaction: any) {
		const newTransaction = transaction instanceof Transaction ? transaction : new Transaction(transaction.amount, transaction.currency);
		this.transactions.push(newTransaction as ITransaction);
		this.balance[newTransaction.currency] = (this.balance[newTransaction.currency] || 0) + newTransaction.amount;
		return newTransaction.getId;
	}

	getTransactions(id?: string) {
		if (id) {
			return this.transactions.filter(transaction => transaction.getId === id);
		} else {
			throw new Error("No transaction id provided");
		}
	}

	getBalance(currency: CurrencyEnum) {
		return this.balance?.[currency] ?? 0;
	}
}