
import {CurrencyEnum, TransactionInterface} from "./types";
const { v4: uuidv4 } = require('uuid');

export default class Transaction implements TransactionInterface {
  #id: string = uuidv4();
  constructor(
    public amount: number,
    public currency: CurrencyEnum,
  ) {
    this.amount = amount;
    this.currency = currency;
  }

  get getId() {
    return this.#id;
  }
}