
import Transaction from "./Transaction";
import {CurrencyEnum, Balance} from "./types";

export default class Card {
	private transactions: Transaction[] = [];

	constructor(
		public balance: Balance = {},
	) {
		this.balance = balance;
	}

	addTransaction(transaction: Transaction): string;
	addTransaction(transaction: {amount: number, currency: CurrencyEnum}): string;
	addTransaction(transaction: any) {
		const newTransaction = transaction instanceof Transaction ? transaction : new Transaction(transaction.amount, transaction.currency);

		this.transactions.push(newTransaction);
		this.balance[newTransaction.currency] = (this.balance[newTransaction.currency] || 0) + newTransaction.amount;
		return newTransaction.getId;
	}

	getTransactions(id?: string) {
		if (id) {
			return this.transactions.filter(transaction => transaction.getId === id);
		} else {
			throw new Error("No transaction id provided");
		}
	}

	getBalance(currency: CurrencyEnum) {
		return currency + ' - ' + (this.balance?.[currency] ?? 0);
	}
}