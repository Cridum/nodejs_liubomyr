

import Card from "./Card";
import Transaction from "./Transaction";
import {CurrencyEnum} from "./types";

const card1 = new Card({USD: 100});
const card2 = new Card({UAH: 100, USD: 200});
const card3 = new Card();

const transaction1 = new Transaction(100, CurrencyEnum.USD);
const transaction2 = new Transaction(50, CurrencyEnum.UAH);
const transaction3 = new Transaction(800, CurrencyEnum.USD);

console.log('card1 - ' + card1.getBalance(CurrencyEnum.USD));
console.log('card2 - ' + card2.getBalance(CurrencyEnum.UAH));
console.log('card2 - ' + card2.getBalance(CurrencyEnum.USD));
console.log('card3 - ' + card3.getBalance(CurrencyEnum.UAH));

card1.addTransaction(transaction1); //add 100$ to card1
card2.addTransaction(transaction2); //add 50UAH to card2
card2.addTransaction(transaction3); //add 800$ to card2
console.log(card3.addTransaction({amount: 500, currency: CurrencyEnum.UAH})); //add 500UAH to card3

console.log('card1 - ' + card1.getBalance(CurrencyEnum.USD));
console.log('card2 - ' + card2.getBalance(CurrencyEnum.UAH));
console.log('card2 - ' + card2.getBalance(CurrencyEnum.USD));
console.log('card3 - ' + card3.getBalance(CurrencyEnum.UAH));

console.log('card1, transaction1 - ' + JSON.stringify(card1.getTransactions(transaction1.getId)));
