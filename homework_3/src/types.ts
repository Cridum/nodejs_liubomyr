import Transaction from "./Transaction";

export enum CurrencyEnum {
    USD = "USD",
    UAH = "UAH",
}

export type Balance = {
    [key in keyof typeof CurrencyEnum as string]?: number;
}

export interface TransactionInterface {
    amount: number;
    currency: CurrencyEnum;
}




