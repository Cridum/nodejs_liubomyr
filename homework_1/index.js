const http = require('http');
const commandLine = require('command-line-args');

const hostname = '127.0.0.1';

const options = [
    { name: 'port', alias: 'p', type: Number, defaultValue: 3000 },
];

const port = commandLine(options).port;

let requestsCount = 0;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    requestsCount++;
    res.setHeader('Content-Type', 'text/plain');
    res.end(JSON.stringify({ 'message': 'Request handled successfully', 'requestCount': requestsCount}));
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});