
import NewPosts from "./newPosts";

const NewPostsTest = new NewPosts();

(async () => {
	try {

		//make some records
		await NewPostsTest.createPost({title: 'New post', text: 'New post text'});
		await NewPostsTest.createPost({title: 'Second New post', text: 'Lorem ipsum'});
		await NewPostsTest.createPost({title: 'Just title'});

		//get all rest records
		const allPosts = await NewPostsTest.getPosts();
		console.log('All posts: ' + JSON.stringify(allPosts, null, 2));

		//get record by id from newsposts table
		const firstPost = await NewPostsTest.getPost(Object.keys(allPosts)[0]);
		console.log('Single post by ID (first post, since ID is auto-generated): ' + JSON.stringify(firstPost, null, 2));

		//modify record by id
		await NewPostsTest.updatePost(Object.keys(allPosts)[1], {title: 'Modified title'});

		//delete record by id
		await NewPostsTest.deletePost(Object.keys(allPosts)[0]);
		//see what left
		console.log('All posts: ' + JSON.stringify(await NewPostsTest.getPosts(), null, 2));

	} catch (error) {
		console.error('Error fetching posts:', error);
	}
})();