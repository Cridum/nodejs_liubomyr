import path from "path";
import { Paths} from "./types";

export const paths: Paths = {
	newsPosts: path.join(__dirname, './../data/newsposts.json'),
}