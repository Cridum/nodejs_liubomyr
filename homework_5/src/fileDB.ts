
import fsp from 'fs/promises';
import {IFileDB} from './types';
import {paths} from './paths';
import {Schema} from "./schemas";

export default class FileDB implements IFileDB {
	errorHandling(err: Error) {
		console.error(err);
		throw err;
	}

	async getTable(tableName: string) {
		try {
			const data = await fsp.readFile(paths[tableName], 'utf-8');
			return JSON.parse(data);
		} catch (err) {
			this.errorHandling(err as Error);
		}
	}

	async getRecord(tableName: string, id: string) {
		try {
			const table = await this.getTable(tableName);
			return table ? table[id] : undefined;
		}
		catch (err) {
			this.errorHandling(err as Error);
		}
	}

	async createRecord(tableName: string, newRecord: object) {
		try {
			const table = await this.getTable(tableName);
			const newTable = [...table, newRecord];
			await fsp.writeFile(paths[tableName], JSON.stringify(newTable, null, 2), 'utf-8');
		} catch (err) {
			this.errorHandling(err as Error);
		}
	}

	async updateRecord(tableName: string, id: string, newRecord: object) {
		try {
			const table = await this.getTable(tableName);
			const index = table.findIndex((record: Schema) => record.id === id);
			table[index] = { ...table[index], ...newRecord };
			await fsp.writeFile(paths[tableName], JSON.stringify(table, null, 2), 'utf-8');
		} catch (err) {
			this.errorHandling(err as Error);
		}
	}

	async deleteRecord(tableName: string, id: string) {
		try {
			const table = await this.getTable(tableName);
			const index = table.findIndex((record: Schema) => record.id === id);
			table.splice(index, 1);
			await fsp.writeFile(paths[tableName], JSON.stringify(table, null, 2), 'utf-8');
		} catch (err) {
			this.errorHandling(err as Error);
		}
	}
};