import FileDB from "./fileDB";
import {v4 as uuidv4} from "uuid";
import {INewPosts} from "./types";
import {NewsPostSchema} from "./schemas";

export default class NewPosts extends FileDB implements INewPosts {
	constructor() {
		super();
	}

	async getPosts() {
		return await super.getTable("newsPosts");
	}

	async getPost(id: string) {
		return await super.getRecord("newsPosts", id);
	}

	async createPost(data: {title?: string, text?: string}){
		const { title, text } = data;
		const newRecord: NewsPostSchema = {
			id: uuidv4(),
			...(title && { title }),
			...(text && { text }),
			date: new Date().toLocaleString(),
		};
		return await super.createRecord("newsPosts", newRecord);
	}

	async updatePost(id: string, data: {title?: string, text?: string}) {
		const { title, text } = data;
		const record = await super.getRecord("newsPosts", id);
		const newRecord = {
			...record,
			...(title && { title }),
			...(text && { text }),
		};
		return await super.updateRecord("newsPosts", id, newRecord);
	}

	public async deletePost(id: string) {
		return await super.deleteRecord("newsPosts", id);
	}
}
