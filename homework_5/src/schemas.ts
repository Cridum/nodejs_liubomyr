
export interface Schemas {
	[key: string]: Schema[];
}

export interface Schema {
	id: string,
	[key: string]: any;
}

export interface NewsPostSchema extends Schema {
	title?: string,
	text?: string,
	date: string,
}

export const schemas: Schemas = {
	newsposts: [] as NewsPostSchema[],
};