
export type Paths = {
	[key: string]: string;
}

export interface IFileDB {
	getTable: (tableName: string) => Promise<object | undefined>;
	getRecord: (tableName: string, id: string) => Promise<object | undefined>;
	createRecord: (tableName: string, newRecord: object) => Promise<void>;
	updateRecord: (tableName: string, id: string, data: object) => Promise<void>;
	deleteRecord: (tableName: string, id: string) => Promise<void>;
	errorHandling: (err: Error) => void;
}

export interface INewPosts {
	getPosts: () => Promise<object | undefined>;
	getPost: (id: string) => Promise<object | undefined>;
	createPost: (data: {title?: string, text?: string}) => Promise<void>;
	updatePost: (id: string, data: {title?: string, text?: string}) => Promise<void>;
	deletePost: (id: string) => Promise<void>;
}
