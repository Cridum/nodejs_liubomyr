const fs = require('fs');
const fsp = fs.promises;
const path = require('path');
const { v4: uuidv4 } = require('uuid');

const fileDB = {
    schemas: {},

    registerSchema: function(name, schema) {
        this.schemas[name] = schema;
    },

    errorHandling: function(err) {
        console.error(err);
        throw err;
    },

    getTable: async function(tableName) {
        const schema = this.schemas[tableName];
        if (!schema) {
            throw new Error(`Schema "${tableName}" is not found`);
        }
        try {
            const data = await fsp.readFile(schema.path, 'utf-8');
            return JSON.parse(data);
        } catch (err) {
            this.errorHandling(err);
        }
    },

    getRecord: async function(tableName, id) {
        try {
            const table = await this.getTable(tableName);
            return table[id];
        }
        catch (err) {
            this.errorHandling(err);
        }
    },

    createRecord: async function(tableName, data) {
        try {
            const table = await this.getTable(tableName);
            const { title, text } = data;
            const newRecord = {
                id: uuidv4(),
                ...(title && { title }),
                ...(text && { text }),
                createDate: new Date().toLocaleString(),
            };
            table[newRecord.id] = newRecord;
            await fsp.writeFile(this.schemas[tableName].path, JSON.stringify(table, null, 2), 'utf-8');
            console.log(`New record with id ${newRecord.id} was created`);
        } catch (err) {
            this.errorHandling(err);
        }
    },

    updateRecord: async function(tableName, id, data) {
        try {
            const table = await this.getTable(tableName);
            const record = table[id];
            if (!record) {
                throw new Error(`Record with id ${id} is not found`);
            }
            const { title, text } = data;
            table[id] = {
                ...record,
                ...(title && {title}),
                ...(text && {text}),
            };
            await fsp.writeFile(this.schemas[tableName].path, JSON.stringify(table, null, 2), 'utf-8');
            console.log(`Record with id ${id} was updated`);
        } catch (err) {
            this.errorHandling(err);
        }
    },

    deleteRecord: async function(tableName, id) {
        try {
            const table = await this.getTable(tableName);
            const record = table[id];
            if (!record) {
                throw new Error(`Record with id ${id} is not found`);
            }
            delete table[id];
            await fsp.writeFile(this.schemas[tableName].path, JSON.stringify(table, null, 2), 'utf-8');
            console.log(`Record with id ${id} was deleted`);
        } catch (err) {
            this.errorHandling(err);
        }
    }
};

const newspostsSchema = {
    postId: {
        id: Number,
        title: String,
        text: String,
        createDate: Date,
    },
    path: path.join(__dirname, './data/newsposts.json')
};

fileDB.registerSchema('newsposts', newspostsSchema);

module.exports = {
    fileDB,
}