const http = require('http');
require('dotenv').config();
const config = require('./config');
const { fileDB } = require('./fileDB');

const hostname = config.get('host');
const port = config.get('port');

(async () => {
    try {

        //make some records
        await fileDB.createRecord('newsposts', {title: 'New post', text: 'New post text'});
        await fileDB.createRecord('newsposts', {title: 'Second New post', text: 'Lorem ipsum', author: 'John Doe'});
        await fileDB.createRecord('newsposts', {title: 'Just title'});

        //get all rest records
        const allPosts = await fileDB.getTable('newsposts');
        console.log('All posts: ' + JSON.stringify(allPosts, null, 2));

        //get record by id from newsposts table
        const firstPost = await fileDB.getRecord('newsposts', Object.keys(allPosts)[0]);
        console.log('Single post by ID (first post, since ID is auto-generated): ' + JSON.stringify(firstPost, null, 2));

        //modify record by id
        await fileDB.updateRecord('newsposts', Object.keys(allPosts)[1], {title: 'Modified title'});

        //delete record by id
        await fileDB.deleteRecord('newsposts', Object.keys(allPosts)[0]);
        //see what left
        console.log('All posts: ' + JSON.stringify(await fileDB.getTable('newsposts'), null, 2));


    } catch (error) {
        console.error('Error fetching posts:', error);
    }
})();

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end(JSON.stringify({ 'message': 'Request handled successfully'}));
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});