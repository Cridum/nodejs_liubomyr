const convict = require("convict");
require("dotenv/config");

const config = convict({
    host: {
        env: "HOST",
        default: "localhost",
        format: String,
        nullable: false,
    },
    port: {
        env: "PORT",
        default: 3000,
        format: Number,
    }
});

config.validate({ allowed: "strict" });
module.exports = config;
